/*
 * picopad, based on rpi pico 
 *
 * Copyright 2023 Pavel Machek <pavel@ucw.cz>
 *
 * This code is licensed under the GPL version 2 or later.  See
 * the COPYING file in the top-level directory.
 */

#include "qemu/osdep.h"
#include "hw/arm/armv7m.h"
#include "qapi/error.h"
#include "hw/boards.h"
#include "hw/sysbus.h"
#include "hw/arm/boot.h"
#include "hw/loader.h"
#include "hw/qdev-properties.h"
#include "hw/qdev-clock.h"
#include "elf.h"
#include "sysemu/reset.h"
#include "qemu/error-report.h"
#include "qemu/module.h"
#include "qemu/log.h"
#include "target/arm/idau.h"
#include "target/arm/cpu-features.h"
#include "migration/vmstate.h"
#include "hw/misc/unimp.h"

#include "sysemu/sysemu.h"
#include "exec/address-spaces.h"
#include "hw/misc/unimp.h"

#include "hw/loader.h"
#include "hw/arm/nrf51_soc.h"
#include "hw/i2c/microbit_i2c.h"
#include "hw/qdev-properties.h"
#include "qom/object.h"

#define RP2040_FLASH_BASE      0x10000000
#define RP2040_FICR_BASE       0x30000000
#define RP2040_FICR_SIZE       0x00000100
#define RP2040_UICR_BASE       0x30001000
#define RP2040_SRAM_BASE       0x20000000

#define RP2040_IOMEM_BASE      0x40000000
#define RP2040_IOMEM_SIZE      0x20000000

#define RP2040_PERIPHERAL_SIZE 0x00001000
#define RP2040_UART_BASE       0x40002000
#define RP2040_TWI_BASE        0x40003000
#define RP2040_TIMER_BASE      0x40008000
#define RP2040_RNG_BASE        0x4000D000
#define RP2040_NVMC_BASE       0x4001E000
#define RP2040_GPIO_BASE       0x50000000

#define RP2040_PRIVATE_BASE    0xF0000000
#define RP2040_PRIVATE_SIZE    0x10000000

#define RP2040_PAGE_SIZE       1024

/* Trigger */
#define RP2040_TRIGGER_TASK 0x01

/* Events */
#define RP2040_EVENT_CLEAR  0x00

#include "hw/sysbus.h"
#include "hw/arm/armv7m.h"
#include "hw/char/nrf51_uart.h"
#include "hw/misc/nrf51_rng.h"
#include "hw/gpio/nrf51_gpio.h"
#include "hw/nvram/nrf51_nvm.h"
#include "hw/timer/nrf51_timer.h"
#include "hw/clock.h"
#include "qom/object.h"

#define TYPE_RP2040_SOC "rp2040-soc"
OBJECT_DECLARE_SIMPLE_TYPE(RP2040State, RP2040_SOC)

#define RP2040_NUM_TIMERS 3

struct RP2040State {
    /*< private >*/
    SysBusDevice parent_obj;

    /*< public >*/
    ARMv7MState cpu;

    NRF51UARTState uart;
#if 0
    NRF51RNGState rng;
    NRF51NVMState nvm;
    NRF51GPIOState gpio;
    NRF51TimerState timer[NRF51_NUM_TIMERS];

    MemoryRegion iomem;
#endif
    MemoryRegion sram;
    MemoryRegion flash;
#if 0
    MemoryRegion clock;
    MemoryRegion twi;
#endif
    uint32_t sram_size;
    uint32_t flash_size;

    MemoryRegion *board_memory;

    MemoryRegion container;

    Clock *sysclk;
};

/*
 * The size and base is for the NRF51822 part. If other parts
 * are supported in the future, add a sub-class of NRF51SoC for
 * the specific variants
 */
#define RP2040_FLASH_PAGES    (2*1024)
#define RP2040_SRAM_PAGES     256
#define RP2040_FLASH_SIZE     (RP2040_FLASH_PAGES * RP2040_PAGE_SIZE)
#define RP2040_SRAM_SIZE      (RP2040_SRAM_PAGES * RP2040_PAGE_SIZE)

#define BASE_TO_IRQ(base) ((base >> 12) & 0x1F)

/* HCLK (the main CPU clock) on this SoC is always 16MHz  FIXME */
#define HCLK_FRQ 16000000

static uint64_t clock_read(void *opaque, hwaddr addr, unsigned int size)
{
    qemu_log_mask(LOG_UNIMP, "%s: 0x%" HWADDR_PRIx " [%u]\n",
                  __func__, addr, size);
    return 1;
}

static void clock_write(void *opaque, hwaddr addr, uint64_t data,
                        unsigned int size)
{
    qemu_log_mask(LOG_UNIMP, "%s: 0x%" HWADDR_PRIx " <- 0x%" PRIx64 " [%u]\n",
                  __func__, addr, data, size);
}

static const MemoryRegionOps clock_ops = {
    .read = clock_read,
    .write = clock_write
};


static void rp2040_soc_realize(DeviceState *dev_soc, Error **errp)
{
    RP2040State *s = RP2040_SOC(dev_soc);
    MemoryRegion *mr;
    Error *err = NULL;
    uint8_t i = 0;
    hwaddr base_addr = 0;

    if (!s->board_memory) {
        error_setg(errp, "memory property was not set");
        return;
    }

    /*
     * HCLK on this SoC is fixed, so we set up sysclk ourselves and
     * the board shouldn't connect it.
     */
    if (clock_has_source(s->sysclk)) {
        error_setg(errp, "sysclk clock must not be wired up by the board code");
        return;
    }
    /* This clock doesn't need migration because it is fixed-frequency */
    clock_set_hz(s->sysclk, HCLK_FRQ);
    qdev_connect_clock_in(DEVICE(&s->cpu), "cpuclk", s->sysclk);
    /*
     * This SoC has no systick device, so don't connect refclk.
     * TODO: model the lack of systick (currently the armv7m object
     * will always provide one).
     */

    object_property_set_link(OBJECT(&s->cpu), "memory", OBJECT(&s->container),
                             &error_abort);
    if (!sysbus_realize(SYS_BUS_DEVICE(&s->cpu), errp)) {
        return;
    }

    memory_region_add_subregion_overlap(&s->container, 0, s->board_memory, -1);

    printf("Adding ram: %d\n", s->sram_size);
    memory_region_init_ram(&s->sram, OBJECT(s), "rp2040.sram", s->sram_size,
                           &err);
    if (err) {
        error_propagate(errp, err);
        return;
    }
    memory_region_add_subregion(&s->container, RP2040_SRAM_BASE, &s->sram);

    printf("Adding flash: %d\n", s->flash_size);
    memory_region_init_ram(&s->flash, OBJECT(s), "rp2040.flash", s->flash_size,
                           &err);
    if (err) {
        error_propagate(errp, err);
        return;
    }
    memory_region_add_subregion(&s->container, RP2040_FLASH_BASE, &s->flash);
    
    /* UART */
    if (!sysbus_realize(SYS_BUS_DEVICE(&s->uart), errp)) {
        return;
    }
    mr = sysbus_mmio_get_region(SYS_BUS_DEVICE(&s->uart), 0);
    memory_region_add_subregion_overlap(&s->container, RP2040_UART_BASE, mr, 0);
    sysbus_connect_irq(SYS_BUS_DEVICE(&s->uart), 0,
                       qdev_get_gpio_in(DEVICE(&s->cpu),
                       BASE_TO_IRQ(RP2040_UART_BASE)));

#if 0
    /* RNG */
    if (!sysbus_realize(SYS_BUS_DEVICE(&s->rng), errp)) {
        return;
    }

    mr = sysbus_mmio_get_region(SYS_BUS_DEVICE(&s->rng), 0);
    memory_region_add_subregion_overlap(&s->container, RP2040_RNG_BASE, mr, 0);
    sysbus_connect_irq(SYS_BUS_DEVICE(&s->rng), 0,
                       qdev_get_gpio_in(DEVICE(&s->cpu),
                       BASE_TO_IRQ(RP2040_RNG_BASE)));

    /* UICR, FICR, NVMC, FLASH */
    if (!object_property_set_uint(OBJECT(&s->nvm), "flash-size",
                                  s->flash_size, errp)) {
        return;
    }

    if (!sysbus_realize(SYS_BUS_DEVICE(&s->nvm), errp)) {
        return;
    }

    mr = sysbus_mmio_get_region(SYS_BUS_DEVICE(&s->nvm), 0);
    memory_region_add_subregion_overlap(&s->container, RP2040_NVMC_BASE, mr, 0);
    mr = sysbus_mmio_get_region(SYS_BUS_DEVICE(&s->nvm), 1);
    memory_region_add_subregion_overlap(&s->container, RP2040_FICR_BASE, mr, 0);
    mr = sysbus_mmio_get_region(SYS_BUS_DEVICE(&s->nvm), 2);
    memory_region_add_subregion_overlap(&s->container, RP2040_UICR_BASE, mr, 0);
    mr = sysbus_mmio_get_region(SYS_BUS_DEVICE(&s->nvm), 3);
    memory_region_add_subregion_overlap(&s->container, RP2040_FLASH_BASE, mr, 0);

    /* GPIO */
    if (!sysbus_realize(SYS_BUS_DEVICE(&s->gpio), errp)) {
        return;
    }

    mr = sysbus_mmio_get_region(SYS_BUS_DEVICE(&s->gpio), 0);
    memory_region_add_subregion_overlap(&s->container, RP2040_GPIO_BASE, mr, 0);

    /* Pass all GPIOs to the SOC layer so they are available to the board */
    qdev_pass_gpios(DEVICE(&s->gpio), dev_soc, NULL);

    /* TIMER */
    for (i = 0; i < RP2040_NUM_TIMERS; i++) {
        if (!object_property_set_uint(OBJECT(&s->timer[i]), "id", i, errp)) {
            return;
        }
        if (!sysbus_realize(SYS_BUS_DEVICE(&s->timer[i]), errp)) {
            return;
        }

        base_addr = RP2040_TIMER_BASE + i * RP2040_PERIPHERAL_SIZE;

        sysbus_mmio_map(SYS_BUS_DEVICE(&s->timer[i]), 0, base_addr);
        sysbus_connect_irq(SYS_BUS_DEVICE(&s->timer[i]), 0,
                           qdev_get_gpio_in(DEVICE(&s->cpu),
                                            BASE_TO_IRQ(base_addr)));
    }

    /* STUB Peripherals */
    memory_region_init_io(&s->clock, OBJECT(dev_soc), &clock_ops, NULL,
                          "rp2040_soc.clock", RP2040_PERIPHERAL_SIZE);
    memory_region_add_subregion_overlap(&s->container,
                                        RP2040_IOMEM_BASE, &s->clock, -1);

    create_unimplemented_device("rp2040_soc.io", RP2040_IOMEM_BASE,
                                RP2040_IOMEM_SIZE);
    create_unimplemented_device("rp2040_soc.private",
                                RP2040_PRIVATE_BASE, RP2040_PRIVATE_SIZE);
#endif
}

static void rp2040_soc_init(Object *obj)
{
    uint8_t i = 0;

    RP2040State *s = RP2040_SOC(obj);

    memory_region_init(&s->container, obj, "rp2040-container", UINT64_MAX);

    object_initialize_child(OBJECT(s), "armv6m", &s->cpu, TYPE_ARMV7M);
    qdev_prop_set_string(DEVICE(&s->cpu), "cpu-type",
                         ARM_CPU_TYPE_NAME("cortex-m0"));
    qdev_prop_set_uint32(DEVICE(&s->cpu), "num-irq", 32);


    object_initialize_child(obj, "uart", &s->uart, TYPE_NRF51_UART);
    object_property_add_alias(obj, "serial0", OBJECT(&s->uart), "chardev");
#if 0
    object_initialize_child(obj, "rng", &s->rng, TYPE_NRF51_RNG);

    object_initialize_child(obj, "nvm", &s->nvm, TYPE_NRF51_NVM);

    object_initialize_child(obj, "gpio", &s->gpio, TYPE_NRF51_GPIO);

    for (i = 0; i < RP2040_NUM_TIMERS; i++) {
        object_initialize_child(obj, "timer[*]", &s->timer[i],
                                TYPE_NRF51_TIMER);

    }
#endif

    s->sysclk = qdev_init_clock_in(DEVICE(s), "sysclk", NULL, NULL, 0);
}

static Property rp2040_soc_properties[] = {
    DEFINE_PROP_LINK("memory", RP2040State, board_memory, TYPE_MEMORY_REGION,
                     MemoryRegion *),
    DEFINE_PROP_UINT32("sram-size", RP2040State, sram_size, RP2040_SRAM_SIZE),
    DEFINE_PROP_UINT32("flash-size", RP2040State, flash_size,
                       RP2040_FLASH_SIZE),
    DEFINE_PROP_END_OF_LIST(),
};

static void rp2040_soc_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);

    dc->realize = rp2040_soc_realize;
    device_class_set_props(dc, rp2040_soc_properties);
}

static const TypeInfo rp2040_soc_info = {
    .name          = "rp2040-soc",
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(RP2040State),
    .instance_init = rp2040_soc_init,
    .class_init    = rp2040_soc_class_init,
};

static void rp2040_soc_types(void)
{
    type_register_static(&rp2040_soc_info);
}
type_init(rp2040_soc_types)

struct PicopadMachineState {
    MachineState parent;

    RP2040State rp2040;
    MicrobitI2CState i2c;
};

#define TYPE_PICOPAD_MACHINE MACHINE_TYPE_NAME("picopad")

OBJECT_DECLARE_SIMPLE_TYPE(PicopadMachineState, PICOPAD_MACHINE)

static void armv7m_reset(void *opaque)
{
    ARMCPU *cpu = opaque;

    cpu_reset(CPU(cpu));

    printf("Jumping to flash\n");
    CPUState *cs = CPU(cpu);
    cpu_set_pc(cs, RP2040_FLASH_BASE);
}

#include "uf2.c"

static void custom_load_kernel(ARMCPU *cpu, const char *kernel_filename,
                               hwaddr mem_base, int mem_size)
{
    ssize_t image_size;
    AddressSpace *as;
    int asidx;
    CPUState *cs = CPU(cpu);

    if (arm_feature(&cpu->env, ARM_FEATURE_EL3)) {
        asidx = ARMASIdx_S;
    } else {
        asidx = ARMASIdx_NS;
    }
    as = cpu_get_address_space(cs, asidx);

    if (kernel_filename) {
        printf("Loading image: %s\n", kernel_filename);
        image_size = load_image_uf2(kernel_filename, mem_base, mem_size, as);

        if (image_size < 0) {
            error_report("Could not load kernel '%s'", kernel_filename);
            exit(1);
        }
    }

    /* CPU objects (unlike devices) are not automatically reset on system
     * reset, so we must always register a handler to do so. Unlike
     * A-profile CPUs, we don't need to do anything special in the
     * handler to arrange that it starts correctly.
     * This is arguably the wrong place to do this, but it matches the
     * way A-profile does it. Note that this means that every M profile
     * board must call this function!
     */
    qemu_register_reset(armv7m_reset, cpu);
}


static void picopad_init(MachineState *machine)
{
    PicopadMachineState *s = PICOPAD_MACHINE(machine);
    MemoryRegion *system_memory = get_system_memory();
    MemoryRegion *mr;

    object_initialize_child(OBJECT(machine), "rp2040", &s->rp2040,
                            TYPE_RP2040_SOC);
    qdev_prop_set_chr(DEVICE(&s->rp2040), "serial0", serial_hd(0));
    object_property_set_link(OBJECT(&s->rp2040), "memory",
                             OBJECT(system_memory), &error_fatal);
    sysbus_realize(SYS_BUS_DEVICE(&s->rp2040), &error_fatal);

    /*
     * Overlap the TWI stub device into the SoC.  This is a microbit-specific
     * hack until we implement the rp2040 TWI controller properly and the
     * magnetometer/accelerometer devices.
     */
    object_initialize_child(OBJECT(machine), "microbit.twi", &s->i2c,
                            TYPE_MICROBIT_I2C);
    sysbus_realize(SYS_BUS_DEVICE(&s->i2c), &error_fatal);
    mr = sysbus_mmio_get_region(SYS_BUS_DEVICE(&s->i2c), 0);
    memory_region_add_subregion_overlap(&s->rp2040.container, RP2040_TWI_BASE,
                                        mr, -1);

    printf("picopad_init: calling load kernel.\n");
    printf("picopad_init: calling load kernel -- name %s.\n", machine->kernel_filename);

    custom_load_kernel(ARM_CPU(first_cpu), machine->kernel_filename,
                       RP2040_FLASH_BASE, s->rp2040.flash_size);
    //s->rp2040.cpu;
}

static void picopad_machine_class_init(ObjectClass *oc, void *data)
{
    MachineClass *mc = MACHINE_CLASS(oc);

    mc->desc = "Picopad (Cortex-M0)";
    mc->init = picopad_init;
    mc->max_cpus = 1;
}

static const TypeInfo picopad_info = {
    .name = TYPE_PICOPAD_MACHINE,
    .parent = TYPE_MACHINE,
    .instance_size = sizeof(PicopadMachineState),
    .class_init = picopad_machine_class_init,
};

static void picopad_machine_init(void)
{
    type_register_static(&picopad_info);
}

type_init(picopad_machine_init);
