struct UF2_Block {
    // 32 byte header
    uint32_t magicStart0;
    uint32_t magicStart1;
    uint32_t flags;
    uint32_t targetAddr;
    uint32_t payloadSize;
    uint32_t blockNo;
    uint32_t numBlocks;
    uint32_t fileSize; // or familyID;
    uint8_t data[476];
    uint32_t magicEnd;
} UF2_Block;

static ssize_t load_image_uf2(const char *filename, hwaddr addr, uint64_t max_sz, AddressSpace *as)
{
    int fd = open(filename, O_RDONLY | O_BINARY);
    const char *oname = "/tmp/code.bin"; /* FIXME */
    ssize_t size;
    static struct UF2_Block uf2;

#if 1
    int out = open(oname, O_WRONLY | O_TRUNC | O_BINARY | O_CREAT, 0700);
    
    while (1) {
        int r = read(fd, &uf2, sizeof(uf2));

        if (r != sizeof(uf2)) {
            if (r != 0) {
                printf("Something very bad reading uf2: %m\n");
                return -1;
            }
            break;
        } 
        if (uf2.magicStart0 != 0x0A324655) {
                printf("Bad magic\n");
                return -1;
            }
        //printf("Block: %lx, %d\n", (unsigned long) uf2.targetAddr, uf2.payloadSize);
        r = write(out, uf2.data, uf2.payloadSize);
        if (r != uf2.payloadSize) {
            printf("Something bad writing: %m\n");
            return -1;
        }
    }
#endif
    size = get_image_size(oname);
    printf("Firmware: %d\n", (int) size);
    if (size < 0 || size > max_sz) {
        return -1;
    }
    if (size > 0) {
        if (rom_add_file_fixed_as(oname, addr, -1, as) < 0) {
            return -1;
        }
    }
    return size;
}
